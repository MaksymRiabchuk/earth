<?php

namespace app\models\enums;

use yii2mod\enum\helpers\BaseEnum;

class PlType extends BaseEnum
{
    const FREE = 'З';
    const PAYER = 'П';

    /**
     * @var array
     */
    public static $list = [
        self::FREE => 'Звільнений',
        self::PAYER => 'Платник',
    ];
}
