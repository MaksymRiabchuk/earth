<?php

namespace app\models\enums;

use yii2mod\enum\helpers\BaseEnum;

class PayerType extends BaseEnum
{
    const TYPE_3WPU = '3WPU';
    const TYPE_4ILG = '4ILG';
    const TYPE_EMPTY = '';
    const TYPE_600S = '600S';
    const TYPE_7EVC = '7EVC';
    const TYPE_60FM = '60FM';
    const TYPE_6VW4 = '6VW4';
    const TYPE_4D46 = '4D46';
    const TYPE_76T7 = '76T7';
    const TYPE_7CK6 = '7CK6';
    /**
     * @var array
     */
    public static $list = [
        self::TYPE_3WPU => '3WPU',
        self::TYPE_4ILG => '4ILG',
        self::TYPE_EMPTY => 'Немає',
        self::TYPE_600S => '600S',
        self::TYPE_7EVC => '7EVC',
        self::TYPE_60FM => '60FM',
        self::TYPE_6VW4 => '6VW4',
        self::TYPE_4D46 => '4D46',
        self::TYPE_76T7 => '76T7',
        self::TYPE_7CK6 => '7CK6',
    ];
}
