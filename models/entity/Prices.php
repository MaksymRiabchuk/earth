<?php

namespace app\models\entity;

use Yii;

/**
 * This is the model class for table "prices".
 *
 * @property int $id
 * @property float|null $price_1
 * @property float|null $price_2
 * @property string|null $zvt
 */
class Prices extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'prices';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['price_1', 'price_2'], 'number'],
            [['zvt'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'price_1' => 'Ціна будівництва',
            'price_2' => 'Ціна ОЗГ',
            'zvt' => 'Zvt',
        ];
    }
}
