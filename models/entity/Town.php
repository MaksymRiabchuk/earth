<?php

namespace app\models\entity;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "town".
 *
 * @property int $id
 * @property string|null $name
 *
 * @property Zem[] $zems
 */
class Town extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'town';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Назва',
        ];
    }

    /**
     * Gets query for [[Zems]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getZems()
    {
        return $this->hasMany(Zem::className(), ['town' => 'id']);
    }
}
