<?php

namespace app\models\entity;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\entity\Zem;

/**
 * ZemSearch represents the model behind the search form of `app\models\entity\Zem`.
 */
class ZemSearch extends Zem
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'no', 'number', 'code', 'town', 'street', 'number_of_house'], 'integer'],
            [['firstname', 'surname', 'parents_name', 'payer', 'pl', 'comment'], 'safe'],
            [['area'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Zem::find()->with('street0');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'no' => $this->no,
            'number' => $this->number,
            'code' => $this->code,
            'town' => $this->town,
            'street' => $this->street,
            'number_of_house' => $this->number_of_house,
            'area' => $this->area,
        ]);

        $query->andFilterWhere(['like', 'firstname', $this->firstname])
            ->andFilterWhere(['like', 'surname', $this->surname])
            ->andFilterWhere(['like', 'parents_name', $this->parents_name])
            ->andFilterWhere(['like', 'payer', $this->payer])
            ->andFilterWhere(['like', 'pl', $this->pl])
            ->andFilterWhere(['like', 'comment', $this->comment]);

        return $dataProvider;
    }
}
