<?php

namespace app\models\entity;

use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "zem".
 *
 * @property int $id
 * @property int|null $no
 * @property int|null $number
 * @property int|null $code
 * @property string|null $firstname
 * @property string|null $surname
 * @property string|null $parents_name
 * @property int|null $town
 * @property int|null $street
 * @property int|null $number_of_house
 * @property float|null $area
 * @property string|null $payer
 * @property string|null $pl
 * @property string|null $comment
 *
 * @property Street $street0
 * @property Town $town0
 */
class Zem extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'zem';
    }
    public function __construct($config = [])
    {
        parent::__construct($config);
        $this->town=1;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['no', 'number', 'code', 'town', 'street', 'number_of_house'], 'integer'],
            [['area'], 'number'],
            [['firstname', 'surname', 'parents_name', 'payer', 'pl', 'comment'], 'string', 'max' => 255],
            [['street'], 'exist', 'skipOnError' => true, 'targetClass' => Street::class, 'targetAttribute' => ['street' => 'id']],
            [['town'], 'exist', 'skipOnError' => true, 'targetClass' => Town::class, 'targetAttribute' => ['town' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'no' => 'No',
            'number' => 'Номер',
            'code' => 'Код',
            'firstname' => 'Прізвище',
            'surname' => 'Ім`я',
            'parents_name' => 'По-батькові',
            'town' => 'Місто',
            'street' => 'Вулиця',
            'number_of_house' => '№ будинку',
            'area' => 'Площа',
            'payer' => 'Код платежу',
            'pl' => 'Платник',
            'comment' => 'Коментар',
        ];
    }

    /**
     * Gets query for [[Street0]].
     *
     * @return ActiveQuery
     */
    public function getStreet0()
    {
        return $this->hasOne(Street::className(), ['id' => 'street']);
    }

    /**
     * Gets query for [[Town0]].
     *
     * @return ActiveQuery
     */
    public function getTown0()
    {
        return $this->hasOne(Town::className(), ['id' => 'town']);
    }

    public function import()
    {
        $streets = Street::getStreetList();
        $f = fopen('2.text', 'r');
        for ($i = 0; $i < 2813; $i++) {
            $buffer = fgets($f, 4096);
            $buffer = substr($buffer, 0, -1);
            $buffer = explode(';', $buffer);
            if (!Zem::find()->where(['no'=>$buffer[0]])->one()){
                $zem = new Zem();
                $zem->no = $buffer[0];
                $zem->number = $buffer[1];
                $zem->code = $buffer[2];
                $zem->firstname = $buffer[3];
                $zem->surname = $buffer[4];
                $zem->parents_name = $buffer[5];
                $key = array_search($buffer[7], $streets);
                if ($key) {
                    $zem->street = $key;
                } else {
                    $zem->street = 1;
                }
                $zem->town = 1;
                $zem->number_of_house = $buffer[8];
                $zem->area = $buffer[9];
                $zem->payer = $buffer[10];
                $zem->pl = $buffer[11];
                $zem->comment = $buffer[12];
                $res = $zem->save();
                if (!$res) {
                    var_dump($zem->errors);
                    die();
                }
            }
        }
        fclose($f);
    }
}
