<?php

namespace app\models\entity;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "street".
 *
 * @property int $id
 * @property string|null $name
 *
 * @property Zem[] $zems
 */
class Street extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'street';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Назва',
        ];
    }

    /**
     * Gets query for [[Zems]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getZems()
    {
        return $this->hasMany(Zem::className(), ['street' => 'id']);
    }
    public function import(){

        $f=fopen('1.text','r');
        for($i=0;$i<81;$i++) {
            $buffer = fgets($f, 4096);
            $buffer = substr($buffer,0,-1);
            if(!Street::find()->where(['name'=>$buffer])->one()){
                $street=new Street();
                $street->name=($buffer);
                $street->save();
            }
        }
        fclose($f);
    }
    public static function getStreetList(){
        return ArrayHelper::map(self::find()->orderBy('name')->all(),'id','name');
    }


}
