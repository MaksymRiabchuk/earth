<?php

use app\models\entity\Street;
use app\models\entity\Zem;
use app\models\enums\PayerType;
use app\models\enums\PlType;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\entity\ZemSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Землекористувачі';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="zem-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <div>
        <?= Html::a('Добавити землекористувача', ['create'], ['class' => 'btn btn-success']) ?>
        <?=  Html::a('Імпортувати', ['import'], ['class' => 'btn btn-danger','style'=>'float:right'])   ?>
        <?=  Html::a('Друкувати 1', ['print'], ['class' => 'btn btn-primary','target'=>'_blank'])   ?>
        <?=  Html::a('Друкувати 2', ['print2'], ['class' => 'btn btn-primary','target'=>'_blank'])   ?>
    </div>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            //'no',
            'number',
            'code',
            'firstname',
            'surname',
            'parents_name',
            [
                    'attribute'=>'street',
                    'filter'=>Street::getStreetList(),
                    'value'=> function($data){
                        /* @var $data Zem */
                        return isset($data->street0)? $data->street0->name : '' ;
                    }
            ],
            'number_of_house',
            'area',
            [
                    'attribute'=>'payer',
                    'filter'=> PayerType::listData(),
            ],
            [
                'attribute'=>'pl',
                'filter'=> PlType::listData(),
                'value'=>function($data){
                    /* @var $data Zem */
                    return PlType::getLabel($data->pl);
                }
            ],
            'comment',

            ['class' => 'yii\grid\ActionColumn','header'=>'&nbsp&nbsp&nbspДії&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp '],
        ],
    ]); ?>


</div>
