<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\entity\Zem */

$this->title = 'Редагувати землекористувача: ' . $model->firstname.' '.$model->surname.' '.$model->parents_name;;
$this->params['breadcrumbs'][] = ['label' => 'Землекористувача', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->firstname.' '.$model->surname.' '.$model->parents_name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редагування';
?>
<div class="zem-update">

    <h2><?= Html::encode($this->title) ?></h2>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
