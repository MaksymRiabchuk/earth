<?php
/* @var $zemList array */

/* @var $item Zem */
/* @var $price Prices */

use app\models\entity\Prices;
use app\models\entity\Street;
use app\models\entity\Zem;
use app\models\enums\PlType;

$streets = Street::getStreetList();
$price = Prices::find()->one();
$price1=$price->price_1;
$price2=$price->price_2;
$areaAll=0;
$budAll=0;
$osgAll=0;
?>
<h1>Список землекористувачів</h1>
<style>
    table {
        width: 100%;
        margin: 0 auto;
    }

    h1 {
        text-align: center;
    }

    th, td {
        border: black solid 1px;
    }

    .no {
        width: 70px;
    }
    .area {
        width: 70px;
    }

    .otg {
        width: 70px;
    }

    .pl {
        width: 90px;
    }

</style>
<table class="table">
    <thead>
    <tr>
        <th rowspan="2">
            Номер
        </th>
        <th rowspan="2">
            Прізвище Ім'я по-батькові
        </th>
        <th rowspan="2">
            Адреса
        </th>
        <th colspan="3">
            Площа
        </th>
        <th rowspan="2">
            Платник
        </th>
        <th rowspan="2">
            Примітка
        </th>
    </tr>
    <tr>
        <th>
            Загальна
        </th>
        <th>
            Будівництво
        </th>
        <th>
            ОТГ
        </th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($zemList as $item): ?>
        <?php $areaAll+=$item->area?>
        <?php $ozg = $item->area - 1500; ?>
        <tr>
            <td class="no">
                <?= $item->no ?>
            </td>
            <td>
                <?= $item->firstname ?> <br> <?= $item->surname ?> <br> <?= $item->parents_name ?>
            </td>
            <td>
                <?= 'смт. Літин' ?> <br> вул. <?= $streets[$item->street] ?> <br> № буд. <?= $item->number_of_house ?>
            </td>
            <td>
                <?= (int)$item->area ?>
            </td>
            <td class="area">
                <?php if ($item->area > 1500): ?>
                    <?= '1500' ?>
                    <?php $areaAll+=1500?>
                <?php else: ?>
                    <?php $budAll+=$item->area?>
                    <?= (int)$item->area ?>
                <?php endif; ?>
            </td>
            <td class="otg">
                <?php if ($item->area > 1500): ?>
                    <?php $osgAll+=$ozg?>
                       <?= $ozg ?>
                <?php else: ?>
                      <?= ' ' ?>
                <?php endif; ?>
            </td>
            <td class="pl">
                <?= PlType::getLabel($item->pl)?>
            </td>
            <td>
                <?= $item->comment?>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
    <tfoot>
        <tr>
            <td>
                Всього: <?= count($zemList)?>
            </td>
            <td colspan="2">
            </td>
            <td>
                <?= $areaAll?>
            </td>
            <td><?=$budAll?></td>
            <td><?=$osgAll?></td>
            <td colspan="2"></td>
        </tr>
    </tfoot>
</table>