<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\entity\Zem */

$this->title = 'Create Zem';
$this->params['breadcrumbs'][] = ['label' => 'Zems', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="zem-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
