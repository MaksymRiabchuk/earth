<?php

use app\models\entity\Zem;
use app\models\enums\PlType;
use yii\helpers\Html;
use yii\web\YiiAsset;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\entity\Zem */

$this->title = $model->firstname.' '.$model->surname.' '.$model->parents_name;
$this->params['breadcrumbs'][] = ['label' => 'Землекористувачі', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
YiiAsset::register($this);
?>
<div class="zem-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редагувати', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Видалити', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Ви дійсно хочете видалити?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'no',
            'number',
            'code',
            'firstname',
            'surname',
            'parents_name',
            [
                'attribute'=>'town',
                'value'=>'смт. Літин',
            ],
            [
                'attribute'=>'street',
                'value'=> function($data){
                    /* @var $data Zem */
                    return isset($data->street0)? $data->street0->name : '' ;
                }
            ],
            'number_of_house',
            'area',
            'payer',
            [
                'attribute'=>'pl',
                'value'=> function($data){
                    /* @var $data Zem */
                    return PlType::getLabel($data->pl) ;
                }
            ],
            'comment',
        ],
    ]) ?>

</div>
