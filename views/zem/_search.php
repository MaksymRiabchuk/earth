<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\entity\ZemSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="zem-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'no') ?>

    <?= $form->field($model, 'number') ?>

    <?= $form->field($model, 'code') ?>

    <?= $form->field($model, 'firstname') ?>

    <?php // echo $form->field($model, 'surname') ?>

    <?php // echo $form->field($model, 'parents_name') ?>

    <?php // echo $form->field($model, 'town') ?>

    <?php // echo $form->field($model, 'street') ?>

    <?php // echo $form->field($model, 'number_of_house') ?>

    <?php // echo $form->field($model, 'area') ?>

    <?php // echo $form->field($model, 'payer') ?>

    <?php // echo $form->field($model, 'pl') ?>

    <?php // echo $form->field($model, 'comment') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
