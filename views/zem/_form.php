<?php

use app\models\entity\Street;
use app\models\enums\PayerType;
use app\models\enums\PlType;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\entity\Zem */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="zem-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'no')->textInput() ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'number')->textInput() ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'code')->textInput() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'firstname')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'surname')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'parents_name')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'street')->dropDownList(Street::getStreetList()) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'number_of_house')->textInput() ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'area')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'payer')->dropDownList(PayerType::listData()) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'pl')->dropDownList(PlType::listData()) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'comment')->textInput(['maxlength' => true]) ?>

        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= Html::submitButton('Зберегти', ['class' => 'btn btn-success']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
