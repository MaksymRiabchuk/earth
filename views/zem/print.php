<?php
/* @var $zemList array */

/* @var $item Zem */

/* @var $price Prices */

use app\models\entity\Prices;
use app\models\entity\Street;
use app\models\entity\Zem;

$streets = Street::getStreetList();
$price = Prices::find()->one();
$price1 = $price->price_1;
$price2 = $price->price_2;
$areaAll = 0;
$budAll = 0;
$osgAll = 0;
?>
<style>
    table {
        width: 100%;
        margin: 0 auto;
    }

    h1 {
        text-align: center;
    }

    th, td {
        border: black solid 1px;
    }

    .no {
        width: 70px;
    }
    .area {
        width: 70px;
    }

    .otg {
        width: 70px;
    }

    .otz {
        width: 70px;
    }

    .summa {
        width: 100px;
    }
</style>
<h1>Список землекористувачів</h1>
<table class="table">
    <thead>
    <tr class="header">
        <th rowspan="2">
            Номер
        </th>
        <th rowspan="2">
            Прізвище Ім'я по-батькові
        </th>
        <th rowspan="2">
            Адреса
        </th>
        <th colspan="3">
            Площа
        </th>
        <th>
            Сума
        </th>
    </tr>
    <tr>
        <th>
            Загальна
        </th>
        <th>
            Будівництво
        </th>
        <th>
            ОТГ
        </th>
        <th>

        </th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($zemList as $item): ?>
        <?php $areaAll += $item->area ?>
        <?php $ozg = $item->area - 1500; ?>
        <tr>
            <td class="no">
                <?= $item->no ?>
            </td>
            <td class="person">
                <?= $item->firstname ?> <br> <?= $item->surname ?> <br> <?= $item->parents_name ?>
            </td>
            <td class="address">
                <?= 'смт. Літин' ?> <br> вул. <?= $streets[$item->street] ?> <br> № буд. <?= $item->number_of_house ?>
            </td>
            <td class="area">
                <?= (int)$item->area ?>
            </td>
            <td class="otg">
                <?php if ($item->area > 1500): ?>
                    <?= '1500' ?>
                    <?php $areaAll += 1500 ?>
                <?php else: ?>
                    <?php $budAll += $item->area ?>
                    <?= (int)$item->area ?>
                <?php endif; ?>
            </td>
            <td class="otz">
                <?php if ($item->area > 1500): ?>
                    <?php $osgAll += $ozg ?>
                    <?= $ozg ?>
                <?php else: ?>
                    <?= ' ' ?>
                <?php endif; ?>
            </td>
            <td class="summa">
                <?php if ($item->area <= 1500): ?>
                    <?= $price1 * $item->area ?>

                <?php else: ?>
                    <?= $price1 * 1500 + ($item->area - 1500) * $price2 ?>
                <?php endif; ?>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
    <tfoot>
    <tr>
        <td>
            Всього: <?= count($zemList) ?>
        </td>
        <td colspan="2">
        </td>
        <td>
            <?= $areaAll ?>
        </td>
        <td><?= $budAll ?></td>
        <td><?= $osgAll ?></td>
        <td><?= $budAll * $price1 + $osgAll * $price2 ?></td>
    </tr>
    </tfoot>
</table>