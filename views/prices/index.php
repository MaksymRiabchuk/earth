<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\entity\PricesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Список цін';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="prices-index">

    <h1><?= Html::encode($this->title) ?></h1>


    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'price_1',
            'price_2',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}',
            ]
        ],
    ]); ?>


</div>
