<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\entity\Prices */

$this->title = 'Редагувати ціни: ';
$this->params['breadcrumbs'][] = ['label' => 'Список цін', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редагувати';
?>
<div class="prices-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
