<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\entity\Town */

$this->title = 'Добавити місто';
$this->params['breadcrumbs'][] = ['label' => 'Список міст', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="town-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
