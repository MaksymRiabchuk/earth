<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\entity\Code */

$this->title = 'Редагувати код: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Список кодів', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редагувати';
?>
<div class="code-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
