<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%zem}}`.
 */
class m210820_135556_create_zem_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%zem}}', [
            'id' => $this->primaryKey(),
            'no'=> $this->integer(),
            'number'=> $this->integer(),
            'code'=> $this->integer(),
            'firstname'=> $this->string(),
            'surname'=> $this->string(),
            'parents_name'=> $this->string(),
            'town'=> $this->integer(),
            'street'=> $this->integer(),
            'number_of_house'=> $this->integer(),
            'area'=> $this->decimal(10,4),
            'payer'=> $this->string(),
            'pl'=>$this->string(),
            'comment'=>$this->string(),
        ]);
        $this->addForeignKey('FK-zem-town-town-id','{{%zem%}}','town','{{%town%}}','id');
        $this->addForeignKey('FK-zem-street-street-id','{{%zem%}}','street','{{%street%}}','id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('FK-zem-street-street-id','{{%zem%}}');
        $this->dropForeignKey('FK-zem-town-town-id','{{%zem%}}');
        $this->dropTable('{{%zem}}');
    }
}
