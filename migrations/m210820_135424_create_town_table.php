<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%town}}`.
 */
class m210820_135424_create_town_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%town}}', [
            'id' => $this->primaryKey(),
            'name'=> $this->string(),
        ]);
        $this->insert('{{%town}}',['id'=>1,'name'=>'смт. Літин']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%town}}');
    }
}
