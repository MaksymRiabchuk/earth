<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%zvt}}`.
 */
class m210820_140558_create_zvt_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%zvt}}', [
            'id' => $this->primaryKey(),
            'code'=> $this->integer(),
            'firstname'=> $this->string(),
            'surname'=> $this->string(),
            'parents_name'=> $this->string(),
            'street'=> $this->integer(),
            'number_of_house'=> $this->integer(),
            'area'=> $this->decimal(10,4),
            'summa'=>$this->decimal(10,2),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%zvt}}');
    }
}
