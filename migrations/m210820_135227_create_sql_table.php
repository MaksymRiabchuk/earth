<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%sql}}`.
 */
class m210820_135227_create_sql_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%sql}}', [
            'id' => $this->primaryKey(),
            'name'=> $this->string(),
            'sql'=> $this->text(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%sql}}');
    }
}
