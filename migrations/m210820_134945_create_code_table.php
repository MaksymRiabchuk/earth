<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%code}}`.
 */
class m210820_134945_create_code_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%code}}', [
            'id' => $this->primaryKey(),
            'code'=> $this->text(),
            'name'=> $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%code}}');
    }
}
